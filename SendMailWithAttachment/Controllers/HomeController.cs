﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using SendMailWithAttachment.Models;

namespace SendMailWithAttachment.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(Mail mailInfo, HttpPostedFileBase attachmentFile)
        {
            if (ModelState.IsValid)

            {

                string from = "zarif.nest@gmail.com"; //example:- sourabh9303@gmail.com

                using (MailMessage mail = new MailMessage(from, mailInfo.To))

                {

                    mail.Subject = mailInfo.Subject;

                    mail.Body = mailInfo.Body;

                    if (attachmentFile != null)

                    {

                        string fileName = Path.GetFileName(attachmentFile.FileName);

                        mail.Attachments.Add(new Attachment(attachmentFile.InputStream, fileName));

                    }

                    mail.IsBodyHtml = false;

                    SmtpClient smtp = new SmtpClient();

                    smtp.Host = "smtp.gmail.com";

                    smtp.EnableSsl = true;

                    NetworkCredential networkCredential = new NetworkCredential(from, "z@rifnest9");

                    smtp.UseDefaultCredentials = true;

                    smtp.Credentials = networkCredential;

                    smtp.Port = 587;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    smtp.Send(mail);


                    ViewBag.Message = "Sent";

                    return View(mailInfo);

                }

            }

            else

            {

                return View();

            }

        }
           

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}